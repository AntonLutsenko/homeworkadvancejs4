// позволяют веб-приложениям работать асинхронно —  обрабатывать запросы отправленные серверу в фоновом режиме. Позволяет отправлять и получать данные с сервера без перезагрузки страницы.

const url = "https://ajax.test-danit.com/api/swapi/films";
const container = document.querySelector(".container");

fetch(url)
	.then((response) => response.json())
	.then((data) => {
		data.map(({ name, episodeId, openingCrawl, characters }) => {
			showEpisod(name, episodeId, openingCrawl);
			showCharacters(characters, episodeId);
		});
	});

const showEpisod = function (name, episodeId, openingCrawl) {
	const listEpisodeElement = document.createElement("div");
	listEpisodeElement.insertAdjacentHTML(
		"afterbegin",
		`<h2>Movie title: "${name}"</h2><ul data-id="episode-${episodeId}"></ul><h3>EPISODE ${episodeId}</h3><span>${openingCrawl}</span>`
	);
	container.append(listEpisodeElement);
};

const addCharacters = function (data, episodeId) {
	const elementCharacters = document.createElement("li");
	elementCharacters.insertAdjacentHTML("afterbegin", `<li>${data.name}</li>`);
	document
		.querySelector(`[data-id="episode-${episodeId}"]`)
		.append(elementCharacters);
};

const showCharacters = function (characters, episodeId) {
	const promises = characters.map((link) =>
		fetch(link).then((response) => response.json())
	);
	Promise.all(promises).then((data) => {
		data.forEach((data) => {
			addCharacters(data, episodeId);
		});
	});
};
